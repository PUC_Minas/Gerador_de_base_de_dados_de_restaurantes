﻿using Facebook;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;

namespace ObterDadosDePaginasDoFacebook {
    class Program {

        /// <summary>
        /// Fornecidos pelo Facebook
        /// </summary>
        private static string AppId = "143425659534114";
        private static string AppSecret = "c628fee1d2df32f2495b7274ecb49aea";

        /// <summary>
        /// Gera o token de acesso da sessão
        /// </summary>
        /// <returns></returns>
        public static string getAccessToken() {
            var client = new WebClient();
            string accessToken = "";
            string oauthUrl = string.Format( "https://graph.facebook.com/oauth/access_token?type=client_cred&client_id={0}&client_secret={1}", AppId, AppSecret );
            dynamic h = null;
            try {
                accessToken = client.DownloadString( oauthUrl );
                h = JsonConvert.DeserializeObject<dynamic>( accessToken );
            } catch (Exception e) {
                Console.WriteLine( e.Message );
            }
            return h.access_token.ToString();
        }

        /// <summary>
        /// Recupera do database o nome do restaurante
        /// </summary>
        /// <param name="line"></param>
        /// <returns></returns>
        public static string getNome(string line) {
            string resp = "";
            int i = 0;
            while (line[ i ] != ',') {
                resp = resp + line[ i ];
                i++;
            }
            return resp;
        }

        /// <summary>
        /// Função principal
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args) {
            string line = "";
            int count = 0;
            List<Page> pag = new List<Page>();
            using (TextWriter tw = new StreamWriter( "database - atualizado.arff" )) {
                using (StreamReader sr = new StreamReader( "database.arff" )) {

                    while (( line = sr.ReadLine() ) != null) {
                        Page p = new Page();
                        long sharesTotal = 0;
                        if (count >= 7) {
                            p = ObterDadosDaPagina( getNome( line ) );
                            if (p != null) {
                                pag.Add( p );
                                if (p.Posts != null) {
                                    foreach (Posts a in p.Posts) {
                                        sharesTotal = sharesTotal + a.SharesCount;
                                    }
                                } else {
                                    p.Posts = new List<Posts>();
                                    sharesTotal = 0;
                                }

                                if (p.PaymentOptions == null) {
                                    p.PaymentOptions = new PaymentOptions();
                                }
                                tw.WriteLine( line + "," + p.LikesCount + "," + p.Checkins + "," + p.EventsCount + "," + p.Posts.Count() + "," + sharesTotal + "," + p.PaymentOptions.Total() + "," + p.PriceRange );
                                Console.WriteLine( "+ " + p.Name );
                            } else {
                                Console.WriteLine( "- " + getNome( line ) );
                            }
                        } else {
                            if (count == 5) {
                                tw.WriteLine( "@ATTRIBUTE likescount numeric" );
                                tw.WriteLine( "@ATTRIBUTE checkins numeric" );
                                tw.WriteLine( "@ATTRIBUTE events numeric" );
                                tw.WriteLine( "@ATTRIBUTE posts numeric" );
                                tw.WriteLine( "@ATTRIBUTE shares numeric" );
                                tw.WriteLine( "@ATTRIBUTE paymentoption numeric" );
                                tw.WriteLine( "@ATTRIBUTE pricerange numeric" );
                                tw.WriteLine( "" );
                            } else {
                                tw.WriteLine( line );
                            }
                        }
                        count++;
                    }
                }
            }
            Console.WriteLine( count + " itens atualizados. Enter para encerrar." );
            Console.ReadLine();
        }

        /// <summary>
        /// Recupera uma lista de paginas com o nome pesquisado
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public static List<Page> ProcuparPagina(String query) {
            List<Page> pageList = new List<Page>();
            try {
                FacebookClient myfbclient = new FacebookClient( getAccessToken() );
                string versio = myfbclient.Version;
                var parameters = new Dictionary<string, object>();

                parameters[ "fields" ] = "id,name";
                dynamic result = null; ;
                result = myfbclient.Get( "search?q=" + query + "&type=page", parameters );
                pageList = new List<Page>();
                int mycount = result.data.Count;

                for (int i = 0; i < result.data.Count; i++) {
                    Page page = new Page() {
                        Id = result.data[ i ].id,
                        Name = result.data[ i ].name
                    };
                    pageList.Add( page );
                }
            } catch (Exception e) {
                Console.WriteLine( "Erro com a pagina escolhida: " + e.Message );
            }
            return pageList;
        }

        /// <summary>
        /// Recupera uma lista com todos os posts já realizados pela pagina
        /// </summary>
        /// <param name="idDaPagina">Nome ou id da pagina</param>
        /// <returns></returns>
        public static List<Posts> ObterPostsDaPagina(String idDaPagina) {
            List<Posts> postsList = new List<Posts>();
            try {
                FacebookClient myfbclient = new FacebookClient( getAccessToken() );
                string versio = myfbclient.Version;
                var parameters = new Dictionary<string, object>();
                parameters[ "fields" ] = "id,message,shares";
                string myPage = idDaPagina;
                dynamic result = null; ;
                result = myfbclient.Get( myPage + "/posts", parameters );
                postsList = new List<Posts>();
                int mycount = result.data.Count;

                for (int i = 0; i < result.data.Count; i++) {
                    Posts posts = new Posts() {
                        PostId = result.data[ i ].id,
                        PostMessage = result.data[ i ].message,
                        SharesCount = result.data[ i ].shares.count
                    };
                    postsList.Add( posts );
                }
            } catch (Exception e) {
            }


            return postsList;

        }

        /// <summary>
        /// Recupera todos os dados quantitativos da pagina
        /// </summary>
        /// <param name="idDaPagina">Nome ou id da pagina</param>
        /// <returns></returns>
        public static Page ObterDadosDaPagina(String idDaPagina) {
            Page pagina = null;
            try {
                pagina = ProcuparPagina( idDaPagina ).FirstOrDefault();
                if (pagina != null) {

                    FacebookClient myfbclient = new FacebookClient( getAccessToken() );
                    string version = myfbclient.Version;
                    var parameters = new Dictionary<string, object>();
                    parameters[ "fields" ] = "id,fan_count,checkins,rating_count,events,price_range,payment_options";
                    dynamic result = null; ;
                    result = myfbclient.Get( pagina.Id + @"/", parameters );
                    pagina.LikesCount = result.fan_count;
                    pagina.Checkins = result.checkins;
                    pagina.PaymentOptions = new PaymentOptions() {
                        Amex = result.payment_options.amex,
                        CashOnly = result.payment_options.cash_only,
                        Discover = result.payment_options.discover,
                        MasterCard = result.payment_options.mastercard,
                        Visa = result.payment_options.visa
                    };
                    pagina.PriceRange = result.price_range.Length;
                    if (result.Count > 6) {
                        pagina.EventsCount = result.events.data.Count;
                    } else {
                        pagina.EventsCount = 0;
                    }
                    pagina.Posts = ObterPostsDaPagina( pagina.Id );
                }
            } catch (Exception e) {
            }

            return pagina;
        }
    }
}
