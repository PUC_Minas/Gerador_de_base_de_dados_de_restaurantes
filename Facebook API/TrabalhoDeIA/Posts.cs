﻿using System;
using System.Drawing;
using System.Linq;

public class Posts {
    public string PostId { get; set; }
    public string PostMessage { get; set; }
    public long SharesCount { get; set; }
}