﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObterDadosDePaginasDoFacebook {
    public class Page {
        public string Id { get; set; }
        public string Name { get; set; }
        public long LikesCount { get; set; }
        public long Checkins { get; set; }
        public long EventsCount { get; set; }
        public PaymentOptions PaymentOptions {get;set;}
        public long PriceRange { get; set; }
        public List<Posts> Posts {get;set;}
    }
}
