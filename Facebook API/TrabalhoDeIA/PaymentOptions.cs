﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObterDadosDePaginasDoFacebook {
    public class PaymentOptions {
        public long Amex { get; set; }
        public long CashOnly { get; set; }
        public long Discover { get; set; }
        public long MasterCard { get; set; }
        public long Visa { get; set; }

        public long Total (){
            return (Amex + CashOnly + Discover + MasterCard + Visa);
        }
    }
}
