# Gerador de base de dados para restaurantes

**Progresso:** CONCLUÍDO<br />
**Autor:** Paulo Victor de Oliveira Leal e Leonardo Palis<br />
**Data:** 2017<br />

### Objetivo

- Desenvolver um software para montar uma base de dados para ser executada no 
Weka (http://www.cs.waikato.ac.nz/ml/weka/).
- Foi desenvolvido em duas partes:
 - Codigo em Python para recuperar restaurantes e outras localidades em Belo
Horizonte e contruir a parte inicial da base de dados.
 - Codigo em C# que recupera no Facebook dados complementares para cada um dos 
restaurantes e localidades gerados pela primeira parte.

### Observação

IDE:  [Visual Studio Code](https://code.visualstudio.com/) e [Visual Studio 2017](https://visualstudio.microsoft.com/)<br />
Linguagem: [C#](https://dotnet.microsoft.com/) e [Python](https://www.python.org/)<br />
Banco de dados: Não utiliza<br />

### Pacotes

| Nome | Função |
| ------ | ------ |
| [Facebook Graph] | API para montar grafos sociais do facebook |

[Facebook Graph]: <https://developers.facebook.com/docs/graph-api/>


### Contribuição

Esse projeto está finalizado e é livre para o uso.

## Licença
<!---

[//]: <> (The Laravel framework is open-sourced software licensed under the [MIT license]https://opensource.org/licenses/MIT)

-->